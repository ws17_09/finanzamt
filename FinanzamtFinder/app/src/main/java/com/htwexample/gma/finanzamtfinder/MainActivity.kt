package com.htwexample.gma.finanzamtfinder

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.widget.*


class MainActivity : Activity() {


         internal var btn_reset : Button? = null
         internal var btn_submit : Button? = null
         var counter = 3
         internal var et_user_name: EditText?= null
         internal var et_password : EditText?= null

         override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState)
         setContentView(R.layout.activity_main)

         // get reference to all views
         et_user_name = findViewById<EditText>(R.id.et_user_name)
         et_password = findViewById<EditText>(R.id.et_password)
         btn_submit = findViewById<Button>(R.id.btn_submit)
         btn_reset = findViewById<Button>(R.id.btn_reset)

             //permission
             if (ActivityCompat.checkSelfPermission(this@MainActivity,
                     android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED )
             {
                 ActivityCompat.requestPermissions(this@MainActivity,
                         arrayOf(android.Manifest.permission.CALL_PHONE),
                         1)
             } else {
                 Log.e("DB", "PERMISSION GRANTED")
             }



        // set on-click



        btn_submit!!.setOnClickListener {

            if( et_user_name!!.getText().toString().equals("admin") &&
                    et_password!!.getText().toString().equals("admin")) {


                val intent = Intent(this, Post::class.java)
                startActivity(intent)


                finish()
            }else{

                Toast.makeText(getApplicationContext(),
                        "wrong  Password  or Username.",Toast.LENGTH_SHORT).show();
                counter--;

                if (counter == 0) {
                    btn_submit!!.setEnabled(false);


                    btn_reset!!.setOnClickListener {
                        // clearing user_name and password edit text views on reset button click
                        et_user_name!!.setText("")
                        et_password!!.setText("")
                        btn_submit!!.setEnabled(true);
                        counter = 3

                    }

                }
            }


        }


    }
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                Log.e("test", "0")
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //yes


                } else {
                    Toast.makeText(this@MainActivity, "you need CAll permission", Toast.LENGTH_SHORT).show()

                }
                return
            }
        }
    }



}



