package com.htwexample.gma.finanzamtfinder

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText




class Post: Activity() {

    internal var suchenbutton: Button?= null
    internal var postLeitzahl: EditText?= null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_layout)

        suchenbutton= findViewById<Button>(R.id.suchen)
        postLeitzahl= findViewById<EditText>(R.id.edit_text1)

        suchenbutton!!.setOnClickListener {




            val intent = Intent(this@Post, suchen::class.java)
            intent.putExtra(AppConstants.VALUE, postLeitzahl!!.text.toString())
            startActivity(intent)




        }
    }


}