package com.htwexample.gma.finanzamtfinder

import android.app.Activity
import android.app.Fragment
import android.content.Intent
import android.os.Bundle
import android.widget.*
import okhttp3.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException



import kotlin.collections.ArrayList
import android.R.id.text1
import android.R.id.edit
import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.preference.PreferenceManager
import android.support.v4.app.ActivityCompat
import android.util.Log


class suchen :  Activity() {
    var model:Model= Model();

    public var tv_name: TextView?=null
    public var tv_email: TextView?= null
    public var tv_strasse: TextView?= null
    public var tv_telefon :TextView?= null
    public var tv_internet:TextView?= null
    public var tv_oeffnungszeiten :TextView?= null
    public var tel :String?=null

    public var FinanzamtEmail:String?=null

    internal   var post2: String?=null
    public var fraglist: ansicht? = null
    var al_details: ArrayList<Model> = ArrayList();
    val client = OkHttpClient()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.adapter_layout)

        run("http://www.berlin.de/sen/finanzen/steuern/finanzaemter/zustaendigkeit-finden/index.php/index.json?page=1")
        fraglist = Fragment.instantiate(this@suchen,
                ansicht::class.java.getName()) as ansicht




    }
fun run(url: String) {

    val request = Request.Builder()
            .url(url)
            .build()

    client.newCall(request).enqueue(object : Callback {
        override fun onFailure(call: Call, e: IOException) {

        }

        override fun onResponse(call: Call, response: Response) {
            var str_response = response.body()!!.string()

            val json_contact:JSONObject = JSONObject(str_response)


            var jsonarray_contacts:JSONArray= json_contact.getJSONArray("index")






            var size:Int = jsonarray_contacts.length()

            al_details = ArrayList();


            val intent = intent
            val value = intent.getStringExtra(AppConstants.VALUE)

            for (i in 0.. size-1) {

                var json_objectdetail:JSONObject=jsonarray_contacts.getJSONObject(i)
                var str1:String =json_objectdetail.getString("bereich")

              if(  str1.toLowerCase().contains(value.toLowerCase())) {

               // var model:Model= Model();
                  model.internet=json_objectdetail.getString("internet")
                  model.email=json_objectdetail.getString("email")
                  model.strasse=json_objectdetail.getString("strasse")
                  model.telefon=json_objectdetail.getString("telefon")
                  model.oeffnungszeiten=json_objectdetail.getString("oeffnungszeiten")
                  model.name=json_objectdetail.getString("name")


                  al_details.add(model)

                  tv_name = findViewById<TextView> (R.id.tv_name)
                  tv_email = findViewById<TextView> (R.id.tv_email)
                  tv_telefon = findViewById<TextView> (R.id.tv_telefon)
                  tv_strasse = findViewById<TextView> (R.id.tv_strasse)
                  tv_oeffnungszeiten= findViewById<TextView> (R.id.tv_oeffnungszeiten)
                  tv_internet = findViewById<TextView> (R.id.tv_internet)
                  tel = model.telefon
                  FinanzamtEmail=model.email



              }
            }



            runOnUiThread {


                tv_name!!.setText(model.name)
                tv_email!!.setText(model.email)
                tv_telefon!!.setText(model.telefon)
                tv_strasse!!.setText(model.strasse)
                tv_internet!!.setText(model.internet)
                tv_oeffnungszeiten!!.setText(model.oeffnungszeiten)

            }

        }






    })

}

 fun    anruf(){





     val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", tel, null))
     startActivity(intent)

 }




    public fun sendEmail() {
        Log.i("Send email", "")
        val TO = arrayOf(FinanzamtEmail)
        val CC = arrayOf("")
        val emailIntent = Intent(Intent.ACTION_SEND)

        emailIntent.data = Uri.parse("mailto:")
        emailIntent.type = "text/plain"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
        emailIntent.putExtra(Intent.EXTRA_CC, CC)
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here")

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."))

            //  Log.i("Finished sending email...", "")
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(this@suchen, "There is no email client installed.", Toast.LENGTH_SHORT).show()
        }



    }


}