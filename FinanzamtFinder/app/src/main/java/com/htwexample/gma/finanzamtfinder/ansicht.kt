package com.htwexample.gma.finanzamtfinder

/**
 * Created by mac on 25.11.17.
 */

import android.app.Fragment
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button


class ansicht: Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        val v = inflater.inflate(R.layout.ansicht, container, false)
        val emailButton = v.findViewById<Button> (R.id.email)
        val mapButton = v.findViewById<Button> (R.id.mapbutton)
        val   callButton = v.findViewById<Button> (R.id.call)


        callButton.setOnClickListener(View.OnClickListener {



            if (activity != null) {
                (activity as suchen).anruf()
            }


        })

        emailButton.setOnClickListener(View.OnClickListener {


            if (activity != null) {
                (activity as suchen).sendEmail()
            }

        })


        mapButton.setOnClickListener(View.OnClickListener {

            val intent = Intent(context,osmmap::class.java)
            startActivity(intent)

        })




        return v

    }








}


