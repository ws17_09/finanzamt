package com.htwexample.gma.finanzamtfinder

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapController
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.ItemizedIconOverlay
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.OverlayItem
import java.util.ArrayList





class osmmap: Activity(){
    private var locationManger: LocationManager? = null
    private var mMapView: MapView? = null
    private var mc: MapController? = null
    internal var overlayItemArrayBburg: ArrayList<OverlayItem>?=null


    //is district selected?
    private var bBChecked: Boolean? = false


    //  private val TAG = OnlineActivityLM::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        // OnlineActivityLM.GetItems().execute()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.map)
        title = "Map"
        org.osmdroid.tileprovider.constants.OpenStreetMapTileProviderConstants.setUserAgentValue(BuildConfig.APPLICATION_ID)

        mMapView = findViewById(R.id.mapview)
        if (mMapView != null) {
            mMapView!!.setTileSource(TileSourceFactory.MAPNIK)
        }

        if (mMapView != null) {
            mMapView!!.setBuiltInZoomControls(true)
        }
        if (mMapView != null) {
            mMapView!!.setMultiTouchControls(true)
        }

        if (mMapView != null) {
            mc = mMapView!!.controller as MapController
        }
        val zoom = 11
        mc!!.setZoom(zoom)
        val berlinLat = 52.520007
        val berlinLng = 13.404954
        val geoPoint = GeoPoint(berlinLat, berlinLng)
        mc!!.animateTo(geoPoint)

        overlayItemArrayBburg = ArrayList()


        //// Add the Array to the IconOverlay
        val itemizedIconOverlay1 = ItemizedIconOverlay(this, overlayItemArrayBburg, null)

        // Add the overlay to the MapView
        mMapView!!.overlays.add(itemizedIconOverlay1)

        // val btn_gps = findViewById(R.id.btn_gps) as Button
        // btn_gps?.setOnClickListener(this)

        //show only the selected districts referring to LMFragment (checkboxes)
        //check if user comes from LMFragment
        //for LM

    }

    fun addMarket(center: GeoPoint) {
        val marker = Marker(mMapView!!)
        marker.position = center
        //set icon for marker
        val icon = getDrawable(R.drawable.person)
        //Set the bounding for the drawable
        icon?.setBounds(0 - icon.intrinsicWidth / 2, 0 - icon.intrinsicHeight, icon.intrinsicWidth / 2, 0)
        //Set the marker to the overlay
        marker.setIcon(icon)
        mMapView!!.overlays.add(marker)
        mMapView!!.invalidate()
    }

    fun onLocationChanged(location: Location) {
        //final double latitude=52.520007;
        //final double longitude=13.404954;
        val center = GeoPoint(location.latitude, location.longitude)
        mc!!.animateTo(center)
        val zoom2 = 20
        mc!!.setZoom(zoom2)
        addMarket(center)
    }

    fun onStatusChanged() {}

    fun onProviderEnabled() {}

    fun onProviderDisabled() {}

    public override fun onDestroy() {
        super.onDestroy()
        if (locationManger != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                return
            }
            // locationManger!!.removeUpdates(this)
        }
    }

    fun map() {




        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // Toast.makeText(this, "Please give Permissions for Calling", Toast.LENGTH_SHORT).show()


        } else {

            locationManger = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            //  locationManger!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, rLocUpd.toLong(), 0f, this)
        }



    }




}


//end of Class